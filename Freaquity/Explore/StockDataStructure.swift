//
//  StockDataStructure.swift
//  Freaquity
//
//  Created by lakshman-7016 on 18/06/21.
//

import Foundation

struct StockList {
	let currency: String
	let company: String
	let ticker: String
	let type: String // can be enum
}

struct StockQuote {
	let currentPrice: Float
	let dayHighPrice: Float
	let dayLowPrice: Float
	let openPrice: Float
	let prevClosePrice: Float
}
