//
//  StocksTableViewCell.swift
//  Freaquity
//
//  Created by Pradeep C on 12/06/21.
//

import UIKit

class StocksTableViewCell: UITableViewCell {

	@IBOutlet weak var stockName: UILabel!
	@IBOutlet weak var stockCurrentValue: UILabel!
	@IBOutlet weak var stockDayChange: UILabel!

	var remoteDataFetcher: RemoteDataFetcher?

	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

//	TODO: Move loadStockQuote method into Model class so that the data can fetched either from remote or local.
	func loadStockQuote(currency: String) {
		self.remoteDataFetcher = RemoteDataFetcher(url: RequestHandler.sharedInstance.getStockQuoteURL(symbol: stockName.text ?? ""))
		func completionHandler(result: LoadDataResult<RemoteStockQuote>) {
			DispatchQueue.main.async {
				if case let .success(remoteStockQuote) = result {
					let stockQuote = remoteStockQuote.toModel()
					let currentValue = stockQuote.currentPrice
					let lastClosedPrice = stockQuote.prevClosePrice
					self.stockCurrentValue.text = currency + String(stockQuote.currentPrice)

					let difference = currentValue - lastClosedPrice
					let differenceInPercentage = difference / lastClosedPrice
					self.stockDayChange.text =  String(format: "%.2f", difference) + " (\(String(format: "%.2f", differenceInPercentage))%"
					self.stockDayChange.textColor = differenceInPercentage < 0 ? .red : .systemGreen
				}
			}
		}
		self.remoteDataFetcher?.load(completion: completionHandler(result:))
	}

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
