//
//  ExploreCollectionViewCell.swift
//  Freaquity
//
//  Created by Pradeep C on 06/06/21.
//

import UIKit

class ExploreCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var callBackgroundView: UIView!
	
	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var codeLabel: UILabel!
	@IBOutlet weak var poitsValueLabel: UILabel!
	@IBOutlet weak var dayChangeLabel: UILabel!
	override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
