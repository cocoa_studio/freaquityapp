//
//  ExploreViewController.swift
//  Freaquity
//
//  Created by Pradeep C on 06/06/21.
//

import UIKit

class ExploreViewController: UIViewController {
	
	var model = ExploreModel()
	
	@IBOutlet weak var stockeExchangeCollectionView: UICollectionView!
	@IBOutlet weak var topGainersCollectionView: UICollectionView!
	@IBOutlet weak var stocksTableView: UITableView!

	let currencySymbol: [String: String] = ["USD": "$"]
	override func viewDidLoad() {
        super.viewDidLoad()
		self.stockeExchangeCollectionView.delegate = self
		self.stockeExchangeCollectionView.dataSource = self
		self.topGainersCollectionView.delegate = self
		self.topGainersCollectionView.dataSource = self
		let stockExchangeNib = UINib(nibName: "ExploreCollectionViewCell", bundle: nil)
		self.stockeExchangeCollectionView.register(stockExchangeNib, forCellWithReuseIdentifier: "ExploreCollectionViewCell")
		let stockNib = UINib(nibName: "StockCollectionViewCell", bundle: nil)
		self.topGainersCollectionView.register(stockNib, forCellWithReuseIdentifier: "StockCollectionViewCell")
		let stockTableCellNib = UINib(nibName: "StocksTableViewCell", bundle: nil)
		self.stocksTableView.register(stockTableCellNib, forCellReuseIdentifier: "Stocks_TableView_Cell")
		self.model.stocksList()
		self.stocksTableView.delegate = self
		self.stocksTableView.dataSource = self
		self.stocksTableView.reloadData()
		self.model.updateUI = {
			DispatchQueue.main.async {
				self.stocksTableView.reloadData()
			}
		}
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ExploreViewController: UICollectionViewDelegate {
	
}

extension ExploreViewController: UICollectionViewDataSource {
	func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
		if self.stockeExchangeCollectionView == collectionView {
			return self.model.stockExchanges.count
		} else if collectionView == self.topGainersCollectionView {
			return self.model.topGainerStocks.count
		}
		return 0
	}
	
	func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
		if self.stockeExchangeCollectionView == collectionView {
			return self.getStockExchangeCollectionViewCell(collectionView: collectionView, indexPath: indexPath)
		} else if collectionView == self.topGainersCollectionView {
			return self.getTopGainerStockCollectionViewCell(collectionView: collectionView, indexPath: indexPath)
		}
		return UICollectionViewCell()
	}
}

extension ExploreViewController {
	func getStockExchangeCollectionViewCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
		guard let cellView = collectionView.dequeueReusableCell(withReuseIdentifier: "ExploreCollectionViewCell", for: indexPath) as? ExploreCollectionViewCell else {
			assertionFailure()
			return UICollectionViewCell()
		}
		cellView.layer.cornerRadius = 10
		cellView.nameLabel.text = self.model.stockExchanges[indexPath.row].name
		let stockExchangeCode = self.model.stockExchanges[indexPath.row].code
		cellView.codeLabel.text = stockExchangeCode
		let currentValue = self.model.getstockExchangeCurrentValue(code: stockExchangeCode)
		cellView.poitsValueLabel.text = String(currentValue)
		let dayChange = self.model.getstockExchangeDayChanges(code: stockExchangeCode)
		let dayChangeValue = currentValue * (dayChange / 100)
		cellView.dayChangeLabel.text = "\(String(format: "%.2f", dayChangeValue)) (\(dayChange)%)"
		return cellView
	}

	func getTopGainerStockCollectionViewCell(collectionView: UICollectionView, indexPath: IndexPath) -> UICollectionViewCell {
		guard let cellView = collectionView.dequeueReusableCell(withReuseIdentifier: "StockCollectionViewCell", for: indexPath) as? StockCollectionViewCell else {
			assertionFailure()
			return UICollectionViewCell()
		}
		cellView.layer.cornerRadius = 10
		cellView.stockNameLabel.text = self.model.topGainerStocks[indexPath.row].name
		let stockCode = self.model.topGainerStocks[indexPath.row].code
		let currentValue = self.model.getStockCurrentValue(code: stockCode)
		cellView.stockCurrentValueLabel.text = String(currentValue)
		let dayChange = self.model.getStockDayChanges(code: stockCode)
		let dayChangeValue = currentValue * (dayChange / 100)
		cellView.stockDayChangeValueLabel.textColor = .systemGreen
		cellView.stockDayChangeValueLabel.text = "\(String(format: "%.2f", dayChangeValue)) (\(dayChange)%)"
		cellView.stockIcon.image = self.model.imageFor(code: stockCode)
		return cellView
	}
}

// MARK: - Stocks TableView
extension ExploreViewController: UITableViewDelegate, UITableViewDataSource {
	func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
		return 70
	}

	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return self.model.stocks.count
	}

//	TODO: Stock list data should not be fetched from remote every time the table view loads.
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let tableViewCell = tableView.dequeueReusableCell(withIdentifier: "Stocks_TableView_Cell") as? StocksTableViewCell else {
			return UITableViewCell()
		}
		let stockSymbolDetails = self.model.stocks[indexPath.row]
		let symbol = stockSymbolDetails.ticker
		let currency = stockSymbolDetails.currency
		if let currencySymbol = self.currencySymbol[currency] {
			tableViewCell.stockName.text = symbol
			tableViewCell.loadStockQuote(currency: currencySymbol)
		}
		return tableViewCell
	}
}
