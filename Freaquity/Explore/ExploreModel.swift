//
//  ExploreModel.swift
//  Freaquity
//
//  Created by Pradeep C on 06/06/21.
//

import Foundation
import UIKit

struct StockExchange {
	var name: String
	var code: String
}
class ExploreModel {
	let requestHandler = RequestHandler.sharedInstance
	lazy var stockListDataFetcher = RemoteDataFetcher(url: self.requestHandler.getStockListURL())
	var stockQuoteDataFetcher: RemoteDataFetcher?
	var stocks: [StockList] = []

	var updateUI: (() -> Void)?
	var stockExchanges: [StockExchange] = [StockExchange(name: "NIFTY 50", code: "NSE"),
									StockExchange(name: "SENSEX", code: "BSE")]
	var stockExchangeCurrentValue: [String: Float] = ["NSE": 15408.3, "BSE": 53081.29]
	var stockExchangeDayChanges: [String: Float] = ["NSE": 0.13, "BSE": 0.92]

	var topGainerStocks: [StockExchange] = [StockExchange(name: "ONGC", code: "ONGC"),
											StockExchange(name: "Bank Of India", code: "BOI"),
											StockExchange(name: "Dabar India", code: "DI"),
											StockExchange(name: "ITC", code: "ITC"),
											StockExchange(name: "Bandhan Bank", code: "BB"),
											StockExchange(name: "Panjab National Bank", code: "PNB")]
	var topLossersStocks: [StockExchange] = [StockExchange(name: "ONGC", code: "ONGC"),
											StockExchange(name: "Bank Of India", code: "BOI"),
											StockExchange(name: "Dabar India", code: "DI"),
											StockExchange(name: "ITC", code: "ITC"),
											StockExchange(name: "Bandhan Bank", code: "BB"),
											StockExchange(name: "Panjab National Bank", code: "PNB")]
	
	var stockCurrentValue: [String: Float] = ["ONGC": 102.3,
													  "BOI": 200.1,
													  "DI": 1409.4,
													  "ITC": 245.0,
													  "BB": 5039.39,
													  "PNB": 10.45]
	var stockDayChangeValue: [String: Float] = ["ONGC": 1.3,
													  "BOI": 0.1,
													  "DI": 0.04,
													  "ITC": 5.0,
													  "BB": 0.01,
													  "PNB": 0.45]
	
	func getstockExchangeCurrentValue(code: String) -> Float {
		guard let currentValue = self.stockExchangeCurrentValue[code] else {
			assertionFailure()
			return -1
		}
		return currentValue
	}

	func getstockExchangeDayChanges(code: String) -> Float {
		guard let currentValue = self.stockExchangeDayChanges[code] else {
			assertionFailure()
			return -1
		}
		return currentValue
	}

	func getStockCurrentValue(code: String) -> Float {
		guard let currentValue = self.stockCurrentValue[code] else {
			assertionFailure()
			return -1
		}
		return currentValue
	}

	func getStockDayChanges(code: String) -> Float {
		guard let currentValue = self.stockDayChangeValue[code] else {
			assertionFailure()
			return -1
		}
		return currentValue
	}

	func imageFor(code: String) -> UIImage? {
		return UIImage(named: code)
	}
}

// MARK: - Stocks
extension ExploreModel {
	func stocksList() {
		// the failure result can be used to display error message in UI
		func completionHandler(result: LoadDataResult<[RemoteStockList]>) {
			if case let .success(stockList) = result {
				self.stocks = stockList.map { $0.toModel() }
				self.updateUI?()
			}
		}
		self.stockListDataFetcher.load(completion: completionHandler(result:))
	}
}
