//
//  StockCollectionViewCell.swift
//  Freaquity
//
//  Created by Pradeep C on 06/06/21.
//

import UIKit

class StockCollectionViewCell: UICollectionViewCell {
	@IBOutlet weak var stockIcon: UIImageView!
	@IBOutlet weak var stockNameLabel: UILabel!
	@IBOutlet weak var stockCurrentValueLabel: UILabel!
	@IBOutlet weak var stockDayChangeValueLabel: UILabel!
	
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
