//
//  RequestHandler.swift
//  Freaquity
//
//  Created by Pradeep C on 12/06/21.
//

import Foundation

class RequestHandler {
	let token = "bse0ecvrh5rea8ra9ipg"
	let baseURL = "finnhub.io"
	let getAllSymbol = "/api/v1/stock/symbol"
	let getStockDetails = "/api/v1/quote"
	static var sharedInstance = RequestHandler()
	func sendReuqest(url: URL,  completionBlock: @escaping (Data) -> Void ) {
		let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
			guard let data = data else {
				print("no data")
				return
			}
			completionBlock(data)
		}

		task.resume()
	}
	
	func getStocksFromServer(completionBlock: @escaping ([NSDictionary]) -> Void ) {
		let marketType = "US"
		var components = URLComponents()
		components.scheme = "https"
		components.host = self.baseURL
		components.path = self.getAllSymbol
		let queryItemToken = URLQueryItem(name: "token", value: self.token)
		let queryItemQuery = URLQueryItem(name: "exchange", value: marketType)
		components.queryItems = [queryItemToken, queryItemQuery]

		guard let url = components.url else {
			assertionFailure("Wrong URL")
			return
		}
		self.sendReuqest(url: url, completionBlock: { stocks in
			do {
				let jsonData = try JSONSerialization.jsonObject(with: stocks, options: .mutableContainers) as? [NSDictionary]
				if var stocksArray = jsonData {
					if stocksArray.count > 50 {
						stocksArray = Array(stocksArray.prefix(50))
					}
					completionBlock(stocksArray)
				}
			} catch let error {
				print(error.localizedDescription)
			}
		})
	}

	func getStockListURL() -> URL {
		let marketType = "US"
		var components = URLComponents()
		components.scheme = "https"
		components.host = self.baseURL
		components.path = self.getAllSymbol
		let queryItemToken = URLQueryItem(name: "token", value: self.token)
		let queryItemQuery = URLQueryItem(name: "exchange", value: marketType)
		components.queryItems = [queryItemToken, queryItemQuery]

		guard let url = components.url else {
			assertionFailure("Wrong URL")
			return URL(string: "")!
		}
		return url
	}

	func getStockQuoteURL(symbol: String) -> URL {
		var components = URLComponents()
		components.scheme = "https"
		components.host = self.baseURL
		components.path = self.getStockDetails
		let queryItemToken = URLQueryItem(name: "token", value: self.token)
		let queryItemQuery = URLQueryItem(name: "symbol", value: symbol)
		components.queryItems = [queryItemToken, queryItemQuery]
		guard let url = components.url else {
			assertionFailure("Wrong URL")
			return URL(string: "")!
		}
		return url
	}

	func getStockValue(symbol: String, completionBlock: @escaping (NSDictionary) -> Void ) {
		var components = URLComponents()
		components.scheme = "https"
		components.host = self.baseURL
		components.path = self.getStockDetails
		let queryItemToken = URLQueryItem(name: "token", value: self.token)
		let queryItemQuery = URLQueryItem(name: "symbol", value: symbol)
		components.queryItems = [queryItemToken, queryItemQuery]
		guard let url = components.url else {
			assertionFailure("Wrong URL")
			return
		}
		self.sendReuqest(url: url, completionBlock: { stocks in
			do {
				let stockDetails = try JSONSerialization.jsonObject(with: stocks, options: .mutableContainers) as? NSDictionary
				if let stockDetails = stockDetails {
					completionBlock(stockDetails)
				}
			} catch let error {
				print(error.localizedDescription)
			}
		})
	}
}
