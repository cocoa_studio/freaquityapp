//
//  ProfileViewController.swift
//  Freaquity
//
//  Created by lakshman-7016 on 06/06/21.
//

import UIKit

class ProfileViewController: UIViewController {
	@IBOutlet weak var profileInfoView: UIView!
	@IBOutlet weak var profilePicView: UIView!
	@IBOutlet weak var peronalInfoView: UIView!

	@IBOutlet weak var profileImageView: UIImageView!


	@IBOutlet weak var nameLabel: UILabel!
	@IBOutlet weak var emailIDLabel: UILabel!
	@IBOutlet weak var phoneNoLabel: UILabel!

	@IBOutlet weak var fundsView: UIView!
	@IBOutlet weak var fundBalanceLabel: UILabel!

	@IBAction func addMoneyButton(_ sender: Any) {
	}

	@IBOutlet weak var profileTableView: UITableView!

	override func viewDidLoad() {
		super.viewDidLoad()
		self.model = ProfileViewModel()
		self.profileTableView.dataSource = self
		let profileTableCellNib = UINib(nibName: "UserAccountDetailsCell", bundle: nil)
		self.profileTableView.register(profileTableCellNib, forCellReuseIdentifier: self.cellID)
		self.configureUI()
		self.loadProfileInfoView()
		self.loadFundsView()
		// Do any additional setup after loading the view.
	}

	var model: ProfileViewModel?

	let cellID = "ProfileDetailsCell"
	var tableViewData = [["All orders", "All SIPs", "Reports"],
						 ["Account details", "Bank and Autopay", "Notifications"]]
	var tableCellIcons = [[UIImage(named: "remindersIcon"),
						   UIImage(named: "calendarIcon"),
						   UIImage(named: "documentIcon")],
						[UIImage(named: "peopleIcon"),
						 UIImage(named: "paymentCardIcon"),
						 UIImage(named: "notificationIcon")]]

	func configureUI() {
		self.profileImageView.layer.cornerRadius = self.profileImageView.frame.height / 2.0
		self.profileImageView.layer.masksToBounds = true
		self.fundsView.layer.shadowOffset = CGSize(width: 1, height: 1)
		self.fundsView.layer.shadowRadius = 2
		self.fundsView.layer.shadowOpacity = 0.3
		self.fundsView.layer.cornerRadius = 5.0
	}

	func loadProfileInfoView() {
		self.nameLabel.text = model?.personalData.name
		self.emailIDLabel.text = model?.personalData.emailId
		self.phoneNoLabel.text = model?.personalData.phoneNo
		self.profileImageView.image = UIImage(named: "Robert")
	}

	func loadFundsView() {
		self.fundBalanceLabel.text = model?.fundBalance
	}

}

extension ProfileViewController: UITableViewDataSource {
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		return tableViewData[section].count
	}

	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		guard let cell = tableView.dequeueReusableCell(withIdentifier: self.cellID, for: indexPath) as? UserAccountDetailsCell else {
			return UITableViewCell()
		}

		cell.iconImageView.image = self.tableCellIcons[indexPath.section][indexPath.row]
		cell.tableCellRowLabel.text = self.tableViewData[indexPath.section][indexPath.row]
//		cell.textLabel!.text = self.tableViewData[indexPath.section][indexPath.row]
		return cell
	}

	func numberOfSections(in tableView: UITableView) -> Int {
		return tableViewData.count
	}

	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		if section == 0 {
			return "My Investments"
		} else {
			return "My Account"
		}
	}

	func tableView(_ tableView: UITableView, titleForFooterInSection section: Int) -> String? {
		if section == 1 {
			return "Freaquity"
		}
		return nil
	}

}
