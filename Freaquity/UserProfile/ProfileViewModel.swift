//
//  ProfileViewModel.swift
//  Freaquity
//
//  Created by lakshman-7016 on 06/06/21.
//

import Foundation

struct PersonalData {
	let id: String
	let name: String
	let emailId: String
	let phoneNo: String

	init(id: String, name: String, emailId: String, phoneNo: String) {
		self.id = id
		self.name = name
		self.emailId = emailId
		self.phoneNo = phoneNo
	}
}

struct Funds {
	var balance: Float = 0.0

	init(balance: Float) {
		self.balance = balance
	}

	mutating func addMoney(value: Float) {
		self.balance += value
	}
}

class ProfileViewModel {

	var personalData: PersonalData = PersonalData(id: "23424", name: "Robert", emailId: "robert@gmail.com", phoneNo: "9332284652")
	var fundsData = Funds(balance: 15.00)

	public var fundBalance: String {
		return "$\(self.fundsData.balance)"
	}

}
