//
//  HTTPClient.swift
//  Freaquity
//
//  Created by lakshman-7016 on 30/06/21.
//

import Foundation

enum HTTPClientResult {
	case success(Data, HTTPURLResponse)
	case failure(Error)
}

protocol HTTPClient {
	func get(from url: URL, completion: @escaping (HTTPClientResult) -> Void)
}
