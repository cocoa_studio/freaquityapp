//
//  APIDataStructures.swift
//  Freaquity
//
//  Created by lakshman-7016 on 25/07/21.
//

import Foundation

///Stock listR
///{
///	"currency": "USD",
///	"description": "UAN POWER CORP",
///	"displaySymbol": "UPOW",
///	"figi": "BBG000BGHYF2",
///	"mic": "OTCM",
///	"symbol": "UPOW",
///	"type": "Common Stock"
///  },
struct RemoteStockList: Decodable, Equatable {
	let currency: String
	let description: String
	let displaySymbol: String
	let figi: String
	let mic: String
	let symbol: String
	let type: String

	func toModel() -> StockList {
		return StockList(currency: self.currency, company: self.description, ticker: self.displaySymbol, type: self.type)
	}
}

///Stock quote
///{
///  "c": 261.74,
///  "h": 263.31,
///  "l": 260.68,
///  "o": 261.07,
///  "pc": 259.45,
///  "t": 1582641000
///}
struct RemoteStockQuote: Decodable, Equatable {
	let c: Float
	let h: Float
	let l: Float
	let o: Float
	let pc: Float
	let t: Double

	func toModel() -> StockQuote {
		return StockQuote(currentPrice: self.c, dayHighPrice: self.h, dayLowPrice: self.l, openPrice: self.o, prevClosePrice: self.pc)
	}
}
