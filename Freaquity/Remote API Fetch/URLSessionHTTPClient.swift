//
//  URLSessionHTTPClient.swift
//  Freaquity
//
//  Created by lakshman-7016 on 01/07/21.
//

import Foundation

class URLSessionHTTPClient: HTTPClient {
	let session: URLSession

	init(session: URLSession = .shared) {
		self.session = session
	}

	func get(from url: URL, completion: @escaping (HTTPClientResult) -> Void) {
		session.dataTask(with: url) { data, response, error in
			if let error = error {
				completion(.failure(error))
			} else if let data = data, let response = response as? HTTPURLResponse {
				completion(.success(data, response))
			}
		}.resume()
	}
}
