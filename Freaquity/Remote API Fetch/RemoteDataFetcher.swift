//
//  RemoteDataFetcher.swift
//  Freaquity
//
//  Created by lakshman-7016 on 19/06/21.
//

import Foundation

// TODO: Modfiy generic type to return domain data model instead of Remote data model.
enum LoadDataResult<T: Decodable> {
	case success(T)
	case failure(Error)
}

// TODO: Add RemoteDataLoader protocol with load method that will be conformed by RemoteDataFetcher

class RemoteDataFetcher {
	let url: URL
	let client: HTTPClient

	public enum Error: Swift.Error {
		case connectivity
		case invalidData
	}

	public typealias Result = LoadDataResult

	init(url: URL, client: HTTPClient = URLSessionHTTPClient()) {
		self.url = url
		self.client = client
	}

	func load<T: Decodable>(completion: @escaping (Result<T>) -> Void) {
		self.client.get(from: self.url) { [weak self] result in
			guard self != nil else { return }

			switch result {
			case let .success(data, response):
				guard response.statusCode == 200,
					  let stockList = try? RemoteDataFetcher.decodeData(to: T.self, data: data) else {
					completion(.failure(Error.invalidData))
					return
				}
				completion(.success(stockList))
			case .failure:
				completion(.failure(Error.connectivity))
			}
		}
	}

	static func decodeData<T: Decodable>(to type: T.Type, data: Data) throws -> T {
		let decoder = JSONDecoder()
		return try decoder.decode(T.self, from: data)
	}
}
