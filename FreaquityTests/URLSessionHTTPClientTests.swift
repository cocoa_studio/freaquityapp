//
//  URLSessionHTTPClentTests.swift
//  FreaquityTests
//
//  Created by lakshman-7016 on 30/06/21.
//

import XCTest
@testable import Freaquity

class URLSessionHTTPClientTests: XCTestCase {
	override func setUp() {
		super.setUp()

		URLProtocolStub.startInterceptingRequests()
	}

	override func tearDown() {
		super.tearDown()

		URLProtocolStub.stopInterceptingRequests()
	}

	func test_getURL_performsRequestWithURL() {
		let url = URL(string: "https://a-given-url.com")!
		let sut = URLSessionHTTPClient()

		let exp = expectation(description: "Wait for completion")
		URLProtocolStub.observeRequests { request in
			XCTAssertEqual(request.url, url)
			exp.fulfill()
		}
		sut.get(from: url) { _ in }
		wait(for: [exp], timeout: 1.0)
	}

	func test_getFromURL_failsOnRequestError() {
		let url = URL(string: "https://a-given-url.com")!
		let sut = URLSessionHTTPClient()

		URLProtocolStub.stub(data: nil, response: nil, error: NSError(domain: "Test", code: 0))
		let exp = expectation(description: "Wait for completion")
		sut.get(from: url) { result in
			switch result {
			case let .failure(receivedError):
				XCTAssertNotNil(receivedError)
			default:
				XCTFail("Expected error, received \(result) instead")
			}
			exp.fulfill()
		}
		wait(for: [exp], timeout: 1.0)
	}

	func test_getFromURL_succeedsOnHTTPURLResponseWithData() {
		let url = URL(string: "https://a-given-url.com")!
		let sut = URLSessionHTTPClient()
		let data = Data("any data".utf8)
		let response = HTTPURLResponse(url: url, statusCode: 200, httpVersion: nil, headerFields: nil)

		URLProtocolStub.stub(data: data, response: response, error: nil)
		let exp = expectation(description: "Wait for completion")
		sut.get(from: url) { result in
			switch result {
			case let .success(receivedData, receivedResponse):
				XCTAssertEqual(receivedResponse.statusCode, response?.statusCode)
				XCTAssertEqual(receivedResponse.url, response?.url)
				XCTAssertEqual(receivedData, data)
			default:
				XCTFail("Expected success response, received \(result) instead")
			}
			exp.fulfill()
		}
		wait(for: [exp], timeout: 1.0)
	}

	class URLProtocolStub: URLProtocol {
		private static var stub: Stub?
		private static var requestObserver: ((URLRequest) -> Void)?

		struct Stub {
			let data: Data?
			let response: HTTPURLResponse?
			let error: Error?
		}

		static func stub(data: Data?, response: HTTPURLResponse?, error: NSError?) {
			self.stub = Stub(data: data, response: response, error: error)
		}

		static func startInterceptingRequests() {
			URLProtocol.registerClass(URLProtocolStub.self)
		}

		static func stopInterceptingRequests() {
			URLProtocol.unregisterClass(URLProtocolStub.self)
		}

		static func observeRequests(observer: @escaping (URLRequest) -> Void) {
			self.requestObserver = observer
		}

		class override func canInit(with request: URLRequest) -> Bool {
			requestObserver?(request)
			return true
		}

		class override func canonicalRequest(for request: URLRequest) -> URLRequest {
			return request
		}

		override func startLoading() {
			if let data = URLProtocolStub.stub?.data {
				client?.urlProtocol(self, didLoad: data)
			}

			if let response = URLProtocolStub.stub?.response {
				client?.urlProtocol(self, didReceive: response, cacheStoragePolicy: .notAllowed)
			}

			if let error = URLProtocolStub.stub?.error {
				client?.urlProtocol(self, didFailWithError: error)
			}

			client?.urlProtocolDidFinishLoading(self)
		}

		override func stopLoading() {}
	}
}
