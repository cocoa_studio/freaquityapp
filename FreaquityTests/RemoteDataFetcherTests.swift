//
//  RemoteDataFetcherTests.swift
//  FreaquityTests
//
//  Created by lakshman-7016 on 19/06/21.
//

import Foundation
import XCTest
@testable import Freaquity

class RemoteDataFetcherTests: XCTestCase {
	func test_init_doesNotRequestDataFromURL() {
		let url = URL(string: "https://a-given-url.com")!
		let (_, client) = makeSUT(url: url)

		XCTAssertTrue(client.requestedURLs.isEmpty)
	}

	func test_load_requestsDataFromURL() {
		let url = URL(string: "https://a-given-url.com")!
		let (sut, client) = makeSUT(url: url)

		sut.load(completion: getEmptyCompletionHandler(_:))

		XCTAssertEqual(client.requestedURLs, [url])
	}

	func test_loadTwice_requestsDataFromURLTwice() {
		let url = URL(string: "https://a-given-url.com")!
		let (sut, client) = makeSUT(url: url)

		sut.load(completion: getEmptyCompletionHandler(_:))
		sut.load(completion: getEmptyCompletionHandler(_:))

		XCTAssertEqual(client.requestedURLs, [url, url])
	}

	func test_load_deliversErrorOnClientErrors() {
		let (sut, client) = makeSUT()
		let clientError = NSError(domain: "Test", code: 0)

		expect(sut, completeWith: getRemoteStockListLoadResultType(with: .connectivity)) {
			client.completeWith(clientError)
		}
	}

	func test_load_deliversErrorOnNon200ClientErrors() {
		let (sut, client) = makeSUT()
		let responseCodes = [199, 201, 300, 400, 404, 500]

		responseCodes.enumerated().forEach { index, statusCode in
			expect(sut, completeWith: getRemoteStockListLoadResultType(with: .invalidData)) {
				client.completeWith(statusCode: statusCode, at: index)
			}
		}
	}

	func test_load_deliversError200HTTPResponseWithInvalidJSON() {
		let (sut, client) = makeSUT()
		let data = Data("invalid json".utf8)

		expect(sut, completeWith: getRemoteStockListLoadResultType(with: .invalidData)) {
			client.completeWith(data: data, statusCode: 200)
		}
	}

	func test_load_deliversNoItemsOn200HTTPResponseWithEmptyJSONList() {
		let (sut, client) = makeSUT()
		let list = [StockList]()
		let data = try! JSONSerialization.data(withJSONObject: list)
		let remoteStockList = [RemoteStockList]()

		expect(sut, completeWith: .success(remoteStockList)) {
			client.completeWith(data: data, statusCode: 200)
		}
	}

	func test_load_deliversItemsOn200HTTPResponseWithJSONList() {
		let (sut, client) = makeSUT()

		let stockJSon = [
			[
				"currency": "USD",
				"description": "APPLE INC",
				"displaySymbol": "AAPL",
				"figi": "BBG000B9Y5X2",
				"mic": "XNGS",
				"symbol": "AAPL",
				"type": "Common Stock"
			],
			[
				"currency": "USD",
				"description": "EXCO TECHNOLOGIES LTD",
				"displaySymbol": "EXCOF",
				"figi": "BBG000JHDDS8",
				"mic": "OOTC",
				"symbol": "EXCOF",
				"type": "Common Stock"
			]
		]

		let remoteStock1 = RemoteStockList(currency: "USD", description: "APPLE INC", displaySymbol: "AAPL", figi: "BBG000B9Y5X2", mic: "XNGS", symbol: "AAPL", type: "Common Stock")
		let remoteStock2 = RemoteStockList(currency: "USD", description: "EXCO TECHNOLOGIES LTD", displaySymbol: "EXCOF", figi: "BBG000JHDDS8", mic: "OOTC", symbol: "EXCOF", type: "Common Stock")

		let remoteStockList = [remoteStock1, remoteStock2]
		let data = try! JSONSerialization.data(withJSONObject: stockJSon)

		expect(sut, completeWith:  .success(remoteStockList)) {
			client.completeWith(data: data, statusCode: 200)
		}
	}

	func test_modelMapping_deliversExpectedResult() {
		let stock1 = StockList(currency: "USD", company: "APPLE INC", ticker: "AAPL", type: "Common Stock")
		let stock2 = StockList(currency: "USD", company: "EXCO TECHNOLOGIES LTD", ticker: "EXCOF", type: "Common Stock")

		let remoteStock1 = RemoteStockList(currency: "USD", description: "APPLE INC", displaySymbol: "AAPL", figi: "BBG000B9Y5X2", mic: "XNGS", symbol: "AAPL", type: "Common Stock")
		let remoteStock2 = RemoteStockList(currency: "USD", description: "EXCO TECHNOLOGIES LTD", displaySymbol: "EXCOF", figi: "BBG000JHDDS8", mic: "OOTC", symbol: "EXCOF", type: "Common Stock")

		let remoteStockList = [remoteStock1, remoteStock2]
		let list = [stock1, stock2]

		remoteStockList.enumerated().forEach { index, remoteStock in
			assertStockListData(stock1: remoteStock.toModel(), stock2: list[index])
		}
	}

	func test_load_doesNotDeliverResultAfterSUTInstanceHasBeenDeallocated() {
		let url = URL(string: "https://a-given-url.com")!
		let client = HTTPClientSpy()
		var sut: RemoteDataFetcher? = RemoteDataFetcher(url: url, client: client)
		let list = [StockList]()
		let data = try! JSONSerialization.data(withJSONObject: list)

		var capturedResult: LoadDataResult<RemoteStockList>?
		sut?.load() { result in
			capturedResult = result
		}

		sut = nil
		
		client.completeWith(data: data, statusCode: 200)
		XCTAssertNil(capturedResult)
	}

	func test_loadStockQuote_deliversQuoteDetailsWith200Response() {
		let (sut, client) = makeSUT()

		let stockQuoteJson = ["c": 261.74,
							  "h": 263.31,
							  "l": 260.68,
							  "o": 261.07,
							  "pc": 259.45,
							  "t": 1582641000 ]
		let data = try! JSONSerialization.data(withJSONObject: stockQuoteJson)

		let expectedQuote = RemoteStockQuote(c: 261.74, h: 263.31, l: 260.68, o: 261.07, pc: 259.45, t: 1582641000)

		self.expect(sut, completeWith: .success(expectedQuote)) {
			client.completeWith(data: data, statusCode: 200)
		}
	}

//	MARK: - Helpers

	func makeSUT(url: URL = URL(string: "https://a-given-url.com")!) -> (RemoteDataFetcher, HTTPClientSpy) {
		let client = HTTPClientSpy()
		return (RemoteDataFetcher(url: url, client: client), client)
	}

	func getRemoteStockListLoadResultType(with error: RemoteDataFetcher.Error) -> LoadDataResult<RemoteStockList> {
		let loadDataResult: LoadDataResult<RemoteStockList> = .failure(error)
		return loadDataResult
	}

	func getEmptyCompletionHandler(_ result: LoadDataResult<RemoteStockList>) {}

	func assertStockListData(stock1: StockList, stock2: StockList) {
		XCTAssertEqual(stock1.currency, stock2.currency)
		XCTAssertEqual(stock1.company, stock2.company)
		XCTAssertEqual(stock1.ticker, stock2.ticker)
		XCTAssertEqual(stock1.type, stock2.type)
	}

	func expect<T: Equatable>(_ sut: RemoteDataFetcher, completeWith expectedResult: LoadDataResult<T>, action: () -> Void, file: StaticString = #filePath, line: UInt = #line) {

		func completionHandler(receivedResult: LoadDataResult<T>) {
			switch (receivedResult, expectedResult) {
			case let (.success(receivedData), .success(expectedData)):
				XCTAssertEqual(receivedData, expectedData, file: file, line: line)
			case let (.failure(receivedError as RemoteDataFetcher.Error), .failure(expectedError as RemoteDataFetcher.Error)):
				XCTAssertEqual(receivedError, expectedError, file: file, line: line)
			default:
				XCTFail("expected \(expectedResult) received \(receivedResult) instead", file: file, line: line)
			}
		}

		sut.load(completion: completionHandler(receivedResult:))

		action()
	}

	class HTTPClientSpy: HTTPClient {
		var requestedURLs = [URL]()
		var completionHandlers = [(HTTPClientResult) -> Void]()

		func get(from url: URL, completion: @escaping (HTTPClientResult) -> Void) {
			self.requestedURLs.append(url)
			self.completionHandlers.append(completion)
		}

		func completeWith(_ error: Error, at index: Int = 0) {
			self.completionHandlers[index](.failure(error))
		}

		func completeWith(data: Data = Data(), statusCode: Int, at index: Int = 0) {
			let response = HTTPURLResponse(url: requestedURLs[index], statusCode: statusCode, httpVersion: nil, headerFields: nil)!
			self.completionHandlers[index](.success(data, response))
		}
	}
}
